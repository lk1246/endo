from django.urls import path
from . import views

app_name = 'endo'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/admit', views.AdmitView.as_view(), name='admit'),
    path('create', views.create, name='create'),
    path('listinput', views.listinput, name='listinput'),
    path('result', views.result, name='result'),
    #path('results/', views.ReslutsView.as_view(), name='results'),

]
