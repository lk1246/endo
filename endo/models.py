from django.db import models
import datetime
from separatedvaluesfield.models import SeparatedValuesField

# Create your models here.

class Constants():
    MALE = "M"
    FEMALE = "F"
    SEX_IN_CHOICE = (
        (MALE, "남자"),
        (FEMALE, "여자"),
    )


    OBST_TOTAL = "TO"
    OBST_PARTIAL = "PO"
    PASSAGE = "PA"
    UNKNOWN = "UK"

    RESULT_IN_SYR = ((OBST_TOTAL, "완전폐쇄"),
                     (OBST_PARTIAL, "불롼전폐쇄"),
                     (PASSAGE, "개통"),
                     (UNKNOWN, "미상 또는 자유기술"),
                     )

    G1 = "G1"
    G2 = "G2"
    G3 = "G3"
    G4 = "G4"
    RESULT_IN_EXAM_CHOICE = ((G1,'Grade1'),
                             (G2, "Grade2"),
                             (G3, "Grade3"),
                             (G4, "Grade4"),
                             )

    UPPER_CANAL = "UC"
    LOWER_CANAL = "LC"
    COMMON_CANAL = "CC"
    SAC = "SAC"
    KRAUSE = "KR"
    NLD = "NLD"
    UNKNOWN = "UK"
    RESULT_OBST_LOC_CHOICE = ((UPPER_CANAL, "상부누소관"),
                              (LOWER_CANAL, "하부누소관"),
                              (COMMON_CANAL, "Rosenmuller valve"),
                              (SAC, "sac"),
                              (KRAUSE,'krause valve'),
                              (NLD, "nasolacrimal duct"),
                              (UNKNOWN, "미상, 또는 자유기술"),
                              )
    MUCUS  = "muc"
    STONE = "stone"
    GRANULOMA = "gra"
    STENOSIS = "steno"
    FIBROSIS = "fibro"
    UNKNOWN = "UK"
    RESULT_OBST_CAUSE_CHOICE = ((MUCUS, "점액질"),
                                (STONE, "누석"),
                                (GRANULOMA, "육아종"),
                                (STENOSIS, "협착"),
                                (FIBROSIS, "섬유화"),
                                (UNKNOWN, "미상, 또는 정상"))




class Patient(models.Model):
    name = models.CharField(max_length=200)
    sex = models.CharField(max_length=1,
                           choices=Constants.SEX_IN_CHOICE,
                           null = True)
    birthday = models.DateField('생년월일', null=True)

    def __str__(self):
        return self.name


class BasicExam(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, null=True)

    exam_date = models.DateField('검사일', null=True)

    tmh_rt = models.CharField("TMH 우안", max_length=8,
                              choices=Constants.RESULT_IN_EXAM_CHOICE,
                              default=Constants.G1,
                              )

    tmh_lt = models.CharField("TMH 좌안", max_length=8,
                              choices=Constants.RESULT_IN_EXAM_CHOICE,
                              default=Constants.G1,
                              )
    ddt_rt = models.CharField("DDT 우안", max_length=8,
                              choices=Constants.RESULT_IN_EXAM_CHOICE,
                              default=Constants.G1,
                              )
    ddt_lt = models.CharField("DDT 좌안", max_length=8,
                              choices=Constants.RESULT_IN_EXAM_CHOICE,
                              default=Constants.G1,
                              )


class Syr(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, null=True)

    exam_date = models.DateField("검사일", null=True)


    syringe_rt = models.CharField("관류검사 우안", max_length=200,
                               choices=Constants.RESULT_IN_SYR,
                               default=Constants.PASSAGE,
                               )
    syringe_lt = models.CharField("관류검사 좌안", max_length=200,
                               choices=Constants.RESULT_IN_SYR,
                               default=Constants.PASSAGE,
                               )



class Dcg(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, null=True)
    exam_date = models.DateField("검사일", null=True)
    obst_loc_rt = SeparatedValuesField("DCG 우안 폐쇄부위",
                                  choices=Constants.RESULT_OBST_LOC_CHOICE,
                                  max_length=100,
                                  null=True
                                       )
    obst_loc_lt = SeparatedValuesField("DCG 좌안 폐쇄부위", max_length=200,
                                  choices=Constants.RESULT_OBST_LOC_CHOICE,
                                   null=True
                                  )
    obst_cau_rt = SeparatedValuesField("DCG 우안 폐쇄원인", max_length=200,
                                  choices=Constants.RESULT_OBST_CAUSE_CHOICE,
                                       null=True)
    obst_cau_lt = SeparatedValuesField("DCG 우안 폐쇄원인", max_length=200,
                                  choices=Constants.RESULT_OBST_CAUSE_CHOICE,
                                  null=True)


class Dacryoscopy(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, null=True)
    exam_date = models.DateField("검사일", null=True)
    obst_loc_rt = SeparatedValuesField("내시경 우안 폐쇄부위",
                                  choices=Constants.RESULT_OBST_LOC_CHOICE,
                                  max_length=100, null=True)
    obst_loc_lt = SeparatedValuesField("내시경 좌안 폐쇄부위", max_length=200,
                                  choices=Constants.RESULT_OBST_LOC_CHOICE,
                                       null=True)
    # obst_cau_rt = SeparatedValuesField("내시경 우안 폐쇄원인", max_length=200,
    #                               choices=Constants.RESULT_OBST_CAUSE_CHOICE,
    #                               )
    # obst_cau_lt = SeparatedValuesField("내시경 우안 폐쇄원인", max_length=200,
    #                               choices=Constants.RESULT_OBST_CAUSE_CHOICE,
    #                               )


class ObsCause(models.Model):
    dacryoscopy = models.ForeignKey(Dacryoscopy, on_delete=models.CASCADE, null=True)

    laterality = models.CharField(max_length=1,
                                  choices=(("R","우"),("L","좌")),
                                  default="R"
                                )

    mucus = models.BooleanField('점액질')
    stone = models.BooleanField('누석')
    granuloma = models.BooleanField('육아종')
    stenosis = models.BooleanField('협착')
    fibrosis = models. BooleanField('섬유화')
    unknown = models.BooleanField('미상/기타')












