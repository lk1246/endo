from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.http import HttpResponseRedirect

from django.db.models import Max, Min, Count

from .models import *

# Create your views here.
class IndexView(generic.ListView):
    template_name = "endo/index.html"
    model = Patient


class DetailView(generic.DetailView):
    model = Patient
    template_name = "endo/detail.html"


class ResultView(generic.DetailView):
    model = Patient
    template_name = "endo/result.html"


class AdmitView(generic.DetailView):
    model = Patient
    template_name = "endo/admit.html"


def create(request):
    patient_p = Patient.objects.create(name= request.POST['name'],
                                       sex=request.POST['sex'],
                                       birthday=request.POST['birthday'])
    try:
        patient_p.save()
    except:
        return render(request, 'endo/listinput.html', { 'error_message': "You made a mistake.",})
    else:

        return HttpResponseRedirect(reverse('endo:index'))


def listinput(request):
    return render(request, "endo/listinput.html")


def result(request):
    a = {}
    a["maxage"] = Patient.objects.all().aggregate(Max('birthday'))["birthday__max"]
    a["minage"] = Patient.objects.all().aggregate(Min('birthday'))["birthday__min"]
    a["count"] = Patient.objects.count()
    return render(request, "endo/result.html", a)