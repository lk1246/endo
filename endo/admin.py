from django.contrib import admin
from .models import *
from nested_inline.admin import NestedTabularInline, NestedModelAdmin

# Register your models here.
class BasicExamInline(NestedTabularInline):
    model = BasicExam
    extra = 1


class SyrInline(NestedTabularInline):
    model = Syr
    extra = 1


class DcgInline(NestedTabularInline):
    model = Dcg
    extra = 1

class ObsCauseInline(NestedTabularInline):
    model = ObsCause
    extra = 2


class DacryoscopyInline(NestedTabularInline):
    model = Dacryoscopy
    extra = 1
    inlines = [ObsCauseInline]






class PatientAdmin(NestedModelAdmin):
    inlines = [BasicExamInline, SyrInline, DcgInline, DacryoscopyInline]


admin.site.register(Patient, PatientAdmin)


